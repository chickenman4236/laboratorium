﻿using ConsoleApp1;

void zad1()
{
    for (int i = 0; i < 32; i++)
    {
        if (i < 16)
        {
            if (i % 8 < 4) Console.Write(">");
            else Console.Write("#");
        }
        else
        {
            if (i % 8 < 4) Console.Write("#");
            else Console.Write("<");
        }
        if (i % 8 == 7) Console.WriteLine();
    }
}
void zad2()
{
    Question[] questions = {
        new Question("Zawsze popierałbym mój kraj bez względu na to, czy ma rację czy nie", 0, -1),
        new Question("Im bardziej wolny rynek tym bardziej wolni ludzie", 1, 0),
        new Question("Rząd powinien mieć władzę nad mediami", 0, -1),
        new Question("Zasoby naturalne należą do ogółu i nie mogą być prywatyzowane", 0, -1),
        new Question("Wzrost gospodarczy to najważniejszy czynnik dobrobytu narodu", 1, 0),
        new Question("Wszelkie narkotyki powinny być zakazane", 0, -1),
        new Question("Dochód, który nie został uzyskany dzięki pracy własnych rąk, powinien być wyżej opodatkowany", -1, 0),
        new Question("Rząd powinien ingerować w rozwój rynku jak najrzadziej to możliwe", 1, 0),
        new Question("Wolna przedsiębiorczość to niezbywalne prawo człowieka", 1, 0),
        new Question("W imię stabilności państwa, rząd musi być w stanie uciszać głosy krytyki", 0, -1),
    };

    int x = 0;
    int y = 0;

    for (int i = 0; i < 10; i++)
    {
        Console.WriteLine();
        Console.WriteLine(questions[i].Content);
        Console.WriteLine("1.Zgadzam się w pełni");
        Console.WriteLine("2.Zgadzam się częściowo");
        Console.WriteLine("3.Nie mam zdania");
        Console.WriteLine("4.Nie zgadzam się");
        Console.WriteLine("5.Całkowicie się nie zgadzam");

        bool again = false;
        do
        {
            if (again) Console.WriteLine("Niepoprawna odpowiedź, spróbuj jeszcze raz");
            ConsoleKeyInfo key = Console.ReadKey();
            Console.WriteLine();

            if (key.KeyChar < '1' || key.KeyChar > '5') again = true;
            else
            {
                int multipler = -(key.KeyChar - '3');
                x += questions[i].Px * multipler;
                y += questions[i].Py * multipler;
                again = false;
            }
        }
        while (again);
    }

    Console.WriteLine("Pozycja na kompasie: ");

    for (int yi = -10; yi <= 10; yi++)
    {
        for (int xi = -10; xi <= 10; xi++)
        {
            if (yi == y && xi == x) Console.Write("X ");
            else
            {
                if (yi == 0) Console.Write("__");
                else if (xi == 0) Console.Write("| ");
                else Console.Write("  ");
            }
        }
        Console.WriteLine();
    }

}

zad1();
zad2();