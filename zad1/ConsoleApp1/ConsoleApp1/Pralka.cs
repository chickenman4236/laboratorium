﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Pralka
    {
        public int currentRpm = 0;
        public int time = 0;
        public bool doorOpened = false;

        public void Start(int rpm, int time)
        {
            if (!doorOpened)
            {
                this.time = time;
                currentRpm = rpm;
            }
        }
        public void Stop()
        {
            currentRpm = 0;
            time = 0;
        }
    }
}
