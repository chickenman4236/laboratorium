﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Odkurzacz
    {
        public bool running = false;
        public float power = 0;
        public bool suction = false;
        
        public void Start(float power, bool suction)
        {
            running = true;
            this.power = power;
            this.suction = suction;
        }
        public void Stop()
        {
            running = false;
        }
    }
}
