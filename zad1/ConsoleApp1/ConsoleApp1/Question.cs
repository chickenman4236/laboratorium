﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Question
    {
        public string Content;
        public int Px;
        public int Py;

        public Question(string content, int px, int py)
        {
            Content = content;
            Py = py;
            Px = px;
        }
    }
}
