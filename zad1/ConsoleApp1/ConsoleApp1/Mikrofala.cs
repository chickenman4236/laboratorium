﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Mikrofala
    {
        public bool doorOpened = false;
        public bool running = false;
        public int time = 0;

        public void Start()
        {
            if(!doorOpened)
                running = true;
        }
        public void Stop()
        {
            running = false;
        }
        public void Open()
        {
            doorOpened = true;
            running = false;
        }
        public void Close()
        {
            doorOpened = false;
        }
    }
}
